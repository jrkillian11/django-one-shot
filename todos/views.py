from django.shortcuts import render
from todos.models import TodoList

# Create your views here.


def todo_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todos_list": todo_list,
    }
    return render(request, "more_list/list.html", context)
